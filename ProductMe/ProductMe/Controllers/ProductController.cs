﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProductMe.Models;

namespace ProductMe.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
       public ActionResult GetData()
        {
            using (DBModel db = new DBModel())
            {
                List<Products> prdList = db.Products.ToList<Products>();
                return Json(new { data = prdList }, JsonRequestBehavior.AllowGet);

            }

        }
    }
}